import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {User} from "../../models/user.model";
import {Pokemon} from "../../models/pokemon.model";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.scss']
})
export class TrainerPage implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  }

  get favourites(): Pokemon[] {
    if(this.userService.user) {
      return this.userService.user.favourites;
    } else {
      return [];
    }
  }

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

}
