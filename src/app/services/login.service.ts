import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map, Observable, of, switchMap} from "rxjs";
import {User} from "../models/user.model";

const apiUsers = environment.apiUsers;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient) { }

  public  login(username: string): Observable<User> {
    return this.checkUsername(username).pipe(
      switchMap((user: User | undefined) => {
        if(user === undefined) {                            //ha nincs user, csináljon
          return this.createUser(username)
        }
        return of(user);                                  //ha van user, adja vissza observable-ként
      })
    )
  }

  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        map((users:User[])  => users.pop())                     //takes the last item out of the array and returns it - now we know there's only one, if array is empty, returns undefined
      )
  }

  private createUser(username: string): Observable<User> {
    const user = {
      username: username,
      favourites: []
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": environment.apiKey
    })

    return this.http.post<User>(apiUsers, user, { headers })
  }
}
