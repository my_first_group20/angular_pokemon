import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Pokemon} from "../models/pokemon.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {finalize} from "rxjs";
import {PokemonData} from "../models/pokemon-data.model";

const apiUrlPokemons = environment.apiPokemons;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: String = "";
  private _loading: boolean = false;

  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void {
    if(this._pokemons.length > 0 || this._loading) {
      return;
    }

    this._loading = true;
    this.http.get<Pokemon[]>(apiUrlPokemons)
      .pipe(
        finalize(() => this._loading = false)
      )
      .subscribe({
        next: (response: any) => {
          let pokemonsFromServer: Pokemon[] = response.results.map((pokemonData: PokemonData) => {

            let urlFragments = pokemonData.url.split("/");                //fragment before last is id
            let id = urlFragments[urlFragments.length-2];

            let newPokemon: Pokemon = {
              id: id,
              name: pokemonData.name,
              url: pokemonData.url,
              img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
            }

            return newPokemon
          })


          this._pokemons = pokemonsFromServer;
        },
        error: (err: HttpErrorResponse) => {
          this._error = err.message;
        }
      })
  }

  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id);
  }

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): String {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }
}
