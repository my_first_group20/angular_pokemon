import { Injectable } from '@angular/core';
import {PokemonCatalogueService} from "./pokemon-catalogue.service";
import {UserService} from "./user.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, tap} from "rxjs";
import {User} from "../models/user.model";
import {environment} from "../../environments/environment";
import {Pokemon} from "../models/pokemon.model";

const apiKey = environment.apiKey;
const apiUsersUrl = environment.apiUsers;

@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  constructor(private readonly pokemonService: PokemonCatalogueService,
              private readonly userService: UserService,
              private readonly http: HttpClient) { }

  public addToFavourites(pokemonId: string): Observable<User> {
    if(!this.userService.user) {
      throw new Error("addToFavourites: No user, can't add to favourites.")
    }

    const user: User = this.userService.user;

    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId)

    if(!pokemon) {
      throw new Error("addToFavourites: No guitar with Id: " + pokemonId);
    }

    if(this.userService.inFavourites(pokemonId)) {
      this.userService.removeFromFavourites(pokemonId)
    } else {
      this.userService.addToFavourites(pokemon)
    }

    const headers = new HttpHeaders({
      "content-type": "application/json",
      "x-api-key": apiKey
    })

    return this.http.patch<User>(`${apiUsersUrl}/${user.id}`, {favourites: [...user.favourites]}, { headers })
      //favourite tömb átadás simán így is jó:
      // return this.http.patch<User>(`${apiUsersUrl}/${user.id}`, {favourites: user.favourites}, { headers })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        })
      )
  }

}
