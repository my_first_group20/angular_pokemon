import {Component, Input, OnInit} from '@angular/core';
import {FavouriteService} from "../../services/favourite.service";
import {HttpErrorResponse} from "@angular/common/http";
import {User} from "../../models/user.model";
import {UserService} from "../../services/user.service";


@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.scss']
})
export class FavouriteButtonComponent implements OnInit {

  @Input() pokemonId: string = "";

  public loading: boolean = false;
  public isFavourite: boolean = false;

  constructor(private readonly favouriteService: FavouriteService,
              private readonly userService: UserService) { }

  ngOnInit(): void {
    this.isFavourite = this.userService.inFavourites(this.pokemonId)
  }

  onFavouriteClick(): void {
    this.loading = true;
    this.favouriteService.addToFavourites(this.pokemonId).subscribe({
      next: (user: User) => {
        console.log(user);
        this.isFavourite = this.userService.inFavourites(this.pokemonId);
        this.loading = false;
      },
      error: (error: HttpErrorResponse) => console.log(error.message)
    })
  }

}
