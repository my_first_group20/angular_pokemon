import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginService} from "../../services/login.service";
import {UserService} from "../../services/user.service";
import {NgForm} from "@angular/forms";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Output() loginEvent: EventEmitter<void> = new EventEmitter();

  constructor(private readonly loginService: LoginService,
              private readonly userService: UserService) { }

  ngOnInit(): void {
  }

  public loginSubmit(loginForm: NgForm): void {

    const { username } = loginForm.value;

    console.log(username)

    this.loginService.login(username).subscribe({
      next: (user: User) => {
        console.log(user)
        this.userService.user = user;
        this.loginEvent.emit();   //továbbnavigáláshoz kisugározzuk az eseményt
      },
      error: error => {}
    })
  }

}
