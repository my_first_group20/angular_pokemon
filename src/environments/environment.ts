export const environment = {
  production: false,
  apiUsers: "https://kpelei-noroff-assignment-api.herokuapp.com/users",
  //éles api végpont:
  // apiPokemons: "https://pokeapi.co/api/v2/pokemon/",
  //json server lokális végpont:
  apiPokemons: "http://localhost:3000/pokemon/",
  apiKey: "HmeUA5C15UOsj2l+Juno0Q=="
};

// JSON server használatához:
// install globally:
//   npm install -g json-server
// run server:
//   json-server --watch json-server/db.json
